Login:
[X] import LoginView (accounts/urls.py)
[X] register LoginView in urlpatterns list with path("login/", LoginView.as_view(), name"login")
[X] create accounts app
[X] create templates directory
-> [X] registration directory
-> [X] login.html
[X] put a "post form" (form="post") in login.html + "fundamental five"
[X] set LOGIN_REDIRECT_URL to "home" (settings.py)
[X] test that login page shows up

Logout:
[X] import LogoutView (accounts/urls.py)
[X] register LogoutView in urlpatterns list path("logout/", LogoutView.as_view(), name"logout")
[X] create html template named "logged_out.html"
[X] put a message in the file about being logged out
[X] test the logout page

Create the sign up view & template
[X] create a function view to handle sign-up form and submission (accounts/urls.py)
-> [X] import the "UserCreationForm" from built-in auth forms
-> [X] use the create_user method to create a new user account w/ username and password
-> [X] use the login function that logs an account in
[X] create the signup.html in registration directory -> form = "post"
[X] register the signup view: path("signup/", signup, "signup")
[X] test the signup view

Create a home page:
[X] create a Django app named "receipts"
[X] install the app in expense/settings.py INSTALLED_APPS ("accounts.apps.AccountsConfig")
[X] create a templates directory in receipts app
-> [X] create a receipts directory in templates
-> [X] create list.html in templates/receipts and put <h1>Home</h1> for now
[ ] create ReceiptListView and have it inherit from LoginRequiredMixin and Receipt model
[X] register ReceiptListView in receipts/urls.py in urlpatterns ("", ReceiptListView.as_view(), name="home")
[X] import RedirectView and register it in expenses/urls.py path("", RedirectView.as_view(url=reverse_lazy("home")))
[X] add links:
-> [X] in base.html - if user is signed in, add a link to url named "logout" w/ content "Logout"
-> [X] in base.html - if user is not signed in, add a link to url named "signup" w/ content "Signup" (need an if statement for whether user.is_authenticated)
-> [X] in base.html - if user is not signed in, add a link to url named "login" w/ content "Login" (need an if statement for whether user.is_authenticated)
[X] test everything that was just made
[X] don't forget to git commmit and push changes to repo

Models:
[X] create ExepnseCategory model: name, owner, a **str**
-> [X] name: models.CharField(max_length=50)
-> [X] owner: models.ForeignKey(User,related_name="categories", models.on_delete="CASCADE")
-> [X] def **str** - returns name value (self.name)
[X] create Account model: name, number, owner, a **str** method
-> [X] name: models.CharField(max_length=100)
-> [X] number: models.CharField(max_length=20)
-> [X] owner: models.ForeignKey("User", related_name="accounts", models.on_delete="CASCADE")
-> [X] def **str** - returns name value (self.name)
[X] create Receipt model: vendor, total, tax, date, purchaser, category, account
-> [X] vendor: models.CharField(max_length=200)
-> [X] total: models.DecimalField(places=3, max=10)
-> [X] tax: models.DecimalField(places=3, max=10)
-> [X] date: models.DateField(auto_now=False)
-> [X] purchaser: models.ForeignKey("AUTH_USER_MODEL", related_name="receipts", on_delete=CASCADE)
-> [X] category: models.ForeignKey("ExpenseCategory", related_name="receipts", related_name="receipts", on_delete=CASCADE)
-> [X] account: models.ForeignKey("Account", related_name="receipts", on_delete=CASCADE, null=True)

Create views:
[ ] receipts/accounts/create to create new receipt account
[ ] receipts/categories/create to create new expense category
[ ] receipts/create to create a new receipt
[ ] if using class views: write your own form_valid method
