from django.urls import path
from .views import (
    ReceiptListView,
    ReceiptCreateView,
    AccountListView,
    AccountCreateView,
    ExpenseCategoryListView,
    ExpenseCategoryCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
    path("accounts/list/", AccountListView.as_view(), name="account_list"),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="account_create"
    ),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="category_list",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="category_create",
    ),
]
